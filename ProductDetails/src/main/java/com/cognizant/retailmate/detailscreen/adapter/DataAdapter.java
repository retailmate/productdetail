package com.cognizant.retailmate.detailscreen.adapter;

/**
 * Created by 599584 on 2/22/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cognizant.retailmate.detailscreen.R;
import com.cognizant.retailmate.detailscreen.model.Imagemodel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<Imagemodel> image_model;
    private Context context;

    public DataAdapter(Context context,ArrayList<Imagemodel> image_model) {
        this.context = context;
        this.image_model=image_model;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.productdetails_imagelist_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        Picasso.with(context).load(image_model.get(i).getImage_url()).resize(120, 60).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return image_model.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView img_android;
        public ViewHolder(View view) {
            super(view);


            img_android = (ImageView)view.findViewById(R.id.image);
        }
    }
}