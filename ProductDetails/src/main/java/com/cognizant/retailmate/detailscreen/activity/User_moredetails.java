package com.cognizant.retailmate.detailscreen.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.cognizant.retailmate.detailscreen.R;

public class User_moredetails extends AppCompatActivity {
    TextView name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_moredetails);

        name = (TextView) findViewById(R.id.name);
        name.setText(getIntent().getStringExtra("name"));
    }
}
