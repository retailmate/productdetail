package com.cognizant.retailmate.detailscreen.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.retailmate.detailscreen.adapter.ProductReviewRecyclerviewAdapter;
import com.cognizant.retailmate.detailscreen.R;
import com.cognizant.retailmate.detailscreen.model.ReviewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */

public class UserReviewFragment extends Fragment {

    //    private GridLayoutManager lLayout;
    private ProductReviewRecyclerviewAdapter productReviewRecyclerviewAdapter;

    List<ReviewModel> reviewList = new ArrayList<>();


    public UserReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_suggestedproduct, container, false);

        RecyclerView review = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);
        TextView count = (TextView) rootView.findViewById(R.id.user_count);


        ReviewModel rmodel = new ReviewModel();

        rmodel.setName("Jack Gibbs");
        rmodel.setDate("12/12/2016");
        rmodel.setReview("I love this Bag!");
        reviewList.add(rmodel);

        rmodel = new ReviewModel();
        rmodel.setName("Mary Gibbs");
        rmodel.setDate("1/2/2017");
        rmodel.setReview("I love this Bag too!");
        reviewList.add(rmodel);

        count.setText(String.valueOf(reviewList.size()));


        productReviewRecyclerviewAdapter = new ProductReviewRecyclerviewAdapter(getContext(), reviewList);
        review.setAdapter(productReviewRecyclerviewAdapter);
        review.setNestedScrollingEnabled(false);
        review.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }


}


