package com.cognizant.retailmate.detailscreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.retailmate.detailscreen.R;
import com.cognizant.retailmate.detailscreen.activity.User_moredetails;
import com.cognizant.retailmate.detailscreen.model.ReviewModel;

import java.util.List;

/**
 * Created by 599584 on 2/21/2017.
 */

public class ProductReviewRecyclerviewAdapter extends RecyclerView.Adapter<ProductReviewRecyclerviewAdapter.MyViewHolder> {

    List<ReviewModel> reviewList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name,date,review;
        LinearLayout layout;

        public MyViewHolder(View view) {
            super(view);
            layout=(LinearLayout)view.findViewById(R.id.layout);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            review = (TextView) view.findViewById(R.id.review);
        }
    }

    public ProductReviewRecyclerviewAdapter(Context context, List<ReviewModel> list){
        this.context=context;
        this.reviewList=list;
    }

    @Override
    public ProductReviewRecyclerviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userreview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final ReviewModel model = reviewList.get(position);

        holder.name.setText(model.getName());
        holder.date.setText(model.getDate());
        holder.review.setText(model.getReview());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,User_moredetails.class);
                intent.putExtra("name",model.getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }
}
