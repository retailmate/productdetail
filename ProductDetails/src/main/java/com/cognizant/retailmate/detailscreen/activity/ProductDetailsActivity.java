package com.cognizant.retailmate.detailscreen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.detailscreen.adapter.ColorAdapter;
import com.cognizant.retailmate.detailscreen.adapter.SuggestedProductRecyclerAdapter;
import com.cognizant.retailmate.detailscreen.adapter.ProductReviewRecyclerviewAdapter;
import com.cognizant.retailmate.detailscreen.adapter.ViewPagerAdapter;
import com.cognizant.retailmate.detailscreen.R;
import com.cognizant.retailmate.detailscreen.model.ColorModel;
import com.cognizant.retailmate.detailscreen.model.ProductModel;
import com.cognizant.retailmate.detailscreen.model.ReviewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailsActivity extends AppCompatActivity {
//    public List<IconData1> data1 = new ArrayList<>();

    public List<ColorModel> colorList = new ArrayList<>();

    Toolbar toolbar;
    ImageView image1;
    TextView r_count, title;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ColorAdapter colorAdapter;
    RecyclerView recyclerView, review;
    SuggestedProductRecyclerAdapter recyclerAdapter1;
    ProductReviewRecyclerviewAdapter productReviewRecyclerviewAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;


    List<ProductModel> productList = new ArrayList<>();

    AppBarLayout appBarLayout;

    List<ReviewModel> reviewList = new ArrayList<>();
    private RecyclerView ColorRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_productdetails);


        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);

        title = (TextView) findViewById(R.id.product_name);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("##$$", String.valueOf(collapsingToolbarLayout.getHeight() + verticalOffset) + " ** " + String.valueOf(2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)));
                if (collapsingToolbarLayout.getHeight() + verticalOffset < 80) {
                    title.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                } else {
                    title.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
            }
        });


        setSupportActionBar(toolbar);
        ImageView imageView = (ImageView) findViewById(R.id.iv);
        Picasso.with(getApplicationContext()).load("http://travelfashiongirl.com/wp-content/uploads/2014/02/How-to-Clean-Your-Travel-Backpack.jpg").into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent act2 = new Intent(view.getContext(), ProductDetailImagesActivity.class);
                startActivity(act2);
            }
        });
        //LinearLayoutManager layoutManager  = new GridLayoutManager(getApplicationContext(),  2);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager layoutManagerColor
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        ColorRecycler = (RecyclerView) findViewById(R.id.colorlist);
        colorAdapter = new ColorAdapter(getApplicationContext(), colorList);
        ColorRecycler.setAdapter(colorAdapter);
        ColorRecycler.setLayoutManager(layoutManagerColor);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        recyclerView.setLayoutManager(layoutManager);

        populateData();


        NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        scrollView.setFillViewport(true);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new UserReviewFragment(), "My Circle Review (" + String.valueOf(reviewList.size()) + ")");
        viewPagerAdapter.addFragments(new Category2(), "User Review");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        recyclerAdapter1 = new SuggestedProductRecyclerAdapter(getApplicationContext(), productList);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recyclerAdapter1);


    }

    private void populateData() {

        ProductModel model = new ProductModel();
        model.setName("Camera");
        model.setPic(R.drawable.sa);
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setPic(R.drawable.sb);
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Camera");
        model.setPic(R.drawable.sa);
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setPic(R.drawable.sb);
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);


        ReviewModel rmodel = new ReviewModel();

        rmodel.setName("Jack Gibbs");
        rmodel.setDate("12/12/2016");
        rmodel.setReview("I love this Bag!");
        reviewList.add(rmodel);

        rmodel = new ReviewModel();
        rmodel.setName("Mary Gibbs");
        rmodel.setDate("1/2/2017");
        rmodel.setReview("I love this Bag too!");
        reviewList.add(rmodel);


        ColorModel colorModel = new ColorModel();
        colorModel.setColor(R.color.colorPrimaryDark);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.black);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.green);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.black);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.colorPrimary);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.colorAccent);
        colorList.add(colorModel);
    }


}