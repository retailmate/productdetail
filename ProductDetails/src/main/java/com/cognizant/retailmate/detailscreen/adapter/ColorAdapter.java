package com.cognizant.retailmate.detailscreen.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.retailmate.detailscreen.R;
import com.cognizant.retailmate.detailscreen.model.ColorModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 599584 on 2/22/2017.
 */

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.MyViewHolder> {

    public List<ColorModel> colorList = new ArrayList<>();
    Context context;
    int selected_position = -1;

    public ColorAdapter(Context context,List<ColorModel> colorList){
        this.context=context;
        this.colorList=colorList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productdetails_color_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ColorModel model = colorList.get(position);
        holder.color.setBackgroundColor(ContextCompat.getColor(context,model.getColor()));
        if(selected_position == position){

            holder.itemView.setBackgroundColor(Color.LTGRAY);
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);


            }
        });



    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public View selected,color;

        public MyViewHolder(View view) {
            super(view);

        selected = (View) view.findViewById(R.id.selected);
            color = (View) view.findViewById(R.id.color);
        }
    }
}

